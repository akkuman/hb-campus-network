# coding=utf-8

"""
湖北飞young移动客户端拨号
注意你的 账号数量 >= macvlan数量
请在下面配置你的 PabdoraBox账号密码 和 飞young账号密码 ，格式如下
ROUTER = {
    'username': 'root', 
    'password': 'admin', 
}
FEIYOUNG = [
    {
        'username': 'xxxxxxxxxxx',
        'password': 'xxxxxx',
    },
    {
        'username': 'xxxxxxxxxxx',
        'password': 'xxxxxx',
    },
    {
        'username': 'xxxxxxxxxxx',
        'password': 'xxxxxx',
    }
]
"""

from datetime import datetime
import hashlib
import requests
import re
import sys
import json
import random
import time

ROUTER = {
    'username': 'root', 
    'password': 'admin', 
}
FEIYOUNG = [
    {
        'username': 'xxxxxxxxxxx',
        'password': 'xxxxxx',
    },
    {
        'username': 'xxxxxxxxxxx',
        'password': 'xxxxxx',
    },
    {
        'username': 'xxxxxxxxxxx',
        'password': 'xxxxxx',
    }
]

class RC4:
    def __init__(self, key = b''):
        """
        Args:
            key: A bytes or a list of int
        """
        if key:
            self._rc4_init(key)

    def _rc4_init(self, key):
        (self.x,self.y) = (0,0)
        key_len = len(key)
        if key_len > 256 or key_len < 1:
            raise IndexError('Invalid key length' + key_len)
        self.state_array = [i for i in range(0,256)] #self.stat_array = range(0,256)
        for i in range(0,256):
            self.x = ((key[i%key_len] & 0xff) + self.state_array[i] + self.x) & 0xff
            self.state_array[i], self.state_array[self.x] = self.state_array[self.x], self.state_array[i]
        (self.x,self.y) = (0,0)

    def engine_crypt(self, input):
        """
        Args:
            input: A bytes
        Returns:
            A result whose type is bytes
        """
        self.out = []
        for i in range(0,len(input)):
            self.x = (self.x + 1) & 0xff
            self.y = (self.state_array[self.x] + self.y) & 0xff
            self.state_array[self.x], self.state_array[self.y] = self.state_array[self.y], self.state_array[self.x]
            self.out.append(input[i] ^ self.state_array[(self.state_array[self.x] + self.state_array[self.y]) & 0xff])
        return bytes(self.out)


class Password:
    def __init__(self, passwd):
        self.passwd = passwd
        self.today_key = self._get_today_key()
    
    def _get_today_key(self):
        """Get the key of the day"""
        day_of_month_key = {
            1: "1430782659",
            2: "0267854319",
            3: "9173268045",
            4: "3401978562",
            5: "8174069325",
            6: "8076142539",
            7: "8957612403",
            8: "4573819602",
            9: "3829507461",
            10: "9356078241",
            11: "4791250368",
            12: "6721895340",
            13: "1938567204",
            14: "4195768023",
            15: "2508479316",
            16: "7029183654",
            17: "1876354092",
            18: "1785043926",
            19: "6178093542",
            20: "5643712089",
            21: "1958627043",
            22: "9572314608",
            23: "0841267953",
            24: "7415038296",
            25: "5364107982",
            26: "1328760549",
            27: "1420698537",
            28: "7368240195",
            29: "8314902567",
            30: "0456897213",
            31: "0954761238",
        }
        return day_of_month_key[datetime.today().day]

    def _md5_hex(self, encode_bytes):
        """HttpImageGetter.encode
        Args:
            encode_bytes: a bytes which is encoded
        Returns:
            A hex string 
        """
        hl = hashlib.md5()
        hl.update(encode_bytes)
        return hl.hexdigest()

    def encrypt(self):
        key = [int(i, 16) for i in self.today_key]  # '123456' ==> [1,2,3,4,5,6]
        return self._md5_hex(RC4(key).engine_crypt(self.passwd.encode()))


class FeiYoungLogin:
    headers = {
        'User-Agent': 'CDMA+WLAN(Maod)',
    }
    def __init__(self, username, password, userip, usermac):
        self.username = str(username)
        self.password = str(password)
        # self.redirect_url = self._get_redirect_url()
        self.redirect_url = 'http://58.53.199.144:8001/?userip=%s&wlanacname=&nasip=58.50.189.124&usermac=%s' % (userip, usermac)
        self.login_url = self._get_login_url()

    def _get_redirect_url(self):
        # 暂时取缔无用了，不需要靠这个方法来获得跳转地址了，上面直接拼接了
        r = requests.get('http://59.37.96.63:80', allow_redirects=False)
        if r.status_code == 200:    # assert r.status_code == 302
            print('[!]You have successfully connected to the network, Please do not retry.')
            return ''
        return r.headers['Location']    #redirect_url = 'http://58.53.199.144:8001/?userip=100.64.224.167&wlanacname=&nasip=58.50.189.124&usermac=1c-87-2c-77-77-9c'
        #return re.search(r'userip=(\S+?)&wlanacname', self.redirect_url).group(1)

    def _get_login_url(self):
        if not self.redirect_url:
            return ''
        r = requests.get(self.redirect_url + '&aidcauthtype=0', headers=self.headers)
        if r.status_code != 200:
            return ''
        return re.search(r'<LoginURL><!\[CDATA\[(\S+?)\]\]></LoginURL>', r.text).group(1)
    
    def do_login(self):
        postdata = {
            'UserName': '!^Maod0%s' % self.username, 
            'Password': Password(self.password).encrypt(), 
            'createAuthorFlag': 0,
        }
        if not self.login_url:
            return '[!]Get LoginURL Failed'
        r = requests.post(self.login_url, data=postdata, headers=self.headers)
        reply_message = re.search(r'<ReplyMessage>(\S+?)</ReplyMessage>', r.text).group(1)
        return '[*]ReplyMessage: %s' % reply_message
    
    @classmethod
    def do_logoff(cls, logoff_url):
        # 多拨中弃用
        r = requests.get(logoff_url, headers=cls.headers)
        return re.search(r'<LogoffReply>([\s\S]+?)</LogoffReply>', r.text).group(1)

class RouterIface:
    # 并未考虑错误处理
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.r_router = self._get_r_router()
        self.reconnect_token = self._get_token()

    def _get_r_router(self):
        auth_data = {
            'luci_username': 'root',
            'luci_password': 'admin',
        }
        # 适用于PandoraBox固件
        r_router = requests.Session()
        r_router.auth = ('admin', 'admin')
        r_router.post('http://192.168.1.1/cgi-bin/luci/', data=auth_data)
        return r_router

    def _get_token(self):
        r_router_net = self.r_router.get('http://192.168.1.1/cgi-bin/luci/admin/network')
        token = re.search(r'name=\"token\" value=\"(.*?)\"', r_router_net.text).group(1)
        return token

    def get_ip_mac(self):
        # 取得network interface的json数据，以生成器的形式返回
        r_router_net = self.r_router.get('http://192.168.1.1/cgi-bin/luci/admin/network')
        iface_ids = re.search(r"XHR\.poll\(\d*, \'/cgi-bin/luci/admin/network/iface_status/(.*)\'", r_router_net.text).group(1)  # lan,vwan1,vwan2,wan,wan6
        vwan_id_list = [i for i in iface_ids.split(',') if 'vwan' in i]  # ['vwan1', 'vwan2']
        for vwan_id in vwan_id_list:
            self._iface_reconnect(vwan_id)
            time.sleep(1)  # 等待一秒进行ip和mac获取
            r_router_iface = self.r_router.get('http://192.168.1.1/cgi-bin/luci/admin/network/iface_status/%s'%vwan_id)
            iface_info_json = json.loads(r_router_iface.text)[0]
            iface_info = {
                'ipaddrs': iface_info_json['ipaddrs'][0][:-3],
                'macaddr': iface_info_json['macaddr'].replace(':', '-').lower(),
            }
            yield iface_info
    
    def _iface_reconnect(self, nameid):
        # 接口重连
        reconnect_data = {
            'token': self.reconnect_token,
        }
        self.r_router.post('http://192.168.1.1/cgi-bin/luci/admin/network/iface_reconnect/%s' % nameid, data=reconnect_data)

    def get_status_iface(self):
        r_router_iface_status = self.r_router.get('http://192.168.1.1/cgi-bin/luci/admin/network/mwan/overview/interface_status')
        router_iface_status_json = json.loads(r_router_iface_status.text)
        iface_status_list = [{'status': vlan['status'], 'name': vlan['name']} for vlan in router_iface_status_json['wans']]
        return iface_status_list

if __name__ == '__main__':

    router_instance = RouterIface(ROUTER['username'], ROUTER['password'])

    # 把feiyoung账号密码列表打乱顺序方便随机取值
    random.shuffle(FEIYOUNG)

    i = 0
    for v in router_instance.get_ip_mac():
        userip = v['ipaddrs']
        usermac = v['macaddr']
        
        # 如果本次取出账号拨号返回信息不是"50：认证成功"就取下一个下一个账号进行本次拨号
        while 1:
            if i > len(FEIYOUNG):
                print("账号已经尝试完，暂无可用")
                break
            feiyoung_username = FEIYOUNG[i]['username']
            feiyoung_password = FEIYOUNG[i]['password']
            reply_message = FeiYoungLogin(feiyoung_username, feiyoung_password, userip, usermac).do_login()
            if '50' in reply_message:
                print(reply_message)
                break
            print("%s！正在尝试下一个账号..."%reply_message)
            i += 1

    # 获取多拨连接状态
    print("请等待几秒后进行状态获取...")
    time.sleep(5)
    status_list = router_instance.get_status_iface()  # [{'status': 'online', 'name': 'vwan1'}, {'status': 'online', 'name': 'vwan2'}]
    for v in status_list:
        print('\tstatus -> %s\n\t name -> %s' % (v['status'], v['name']))