# coding=utf-8

"""
湖北飞young移动客户端拨号
"""

from datetime import datetime
import hashlib
import requests
import re
import sys


class RC4:
    def __init__(self, key = b''):
        """
        Args:
            key: A bytes or a list of int
        """
        if key:
            self._rc4_init(key)

    def _rc4_init(self, key):
        (self.x,self.y) = (0,0)
        key_len = len(key)
        if key_len > 256 or key_len < 1:
            raise IndexError('Invalid key length' + key_len)
        self.state_array = [i for i in range(0,256)] #self.stat_array = range(0,256)
        for i in range(0,256):
            self.x = ((key[i%key_len] & 0xff) + self.state_array[i] + self.x) & 0xff
            self.state_array[i], self.state_array[self.x] = self.state_array[self.x], self.state_array[i]
        (self.x,self.y) = (0,0)

    def engine_crypt(self, input):
        """
        Args:
            input: A bytes
        Returns:
            A result whose type is bytes
        """
        self.out = []
        for i in range(0,len(input)):
            self.x = (self.x + 1) & 0xff
            self.y = (self.state_array[self.x] + self.y) & 0xff
            self.state_array[self.x], self.state_array[self.y] = self.state_array[self.y], self.state_array[self.x]
            self.out.append(input[i] ^ self.state_array[(self.state_array[self.x] + self.state_array[self.y]) & 0xff])
        return bytes(self.out)


class Password:
    def __init__(self, passwd):
        self.passwd = passwd
        self.today_key = self._get_today_key()
    
    def _get_today_key(self):
        """Get the key of the day"""
        day_of_month_key = {
            1: "1430782659",
            2: "0267854319",
            3: "9173268045",
            4: "3401978562",
            5: "8174069325",
            6: "8076142539",
            7: "8957612403",
            8: "4573819602",
            9: "3829507461",
            10: "9356078241",
            11: "4791250368",
            12: "6721895340",
            13: "1938567204",
            14: "4195768023",
            15: "2508479316",
            16: "7029183654",
            17: "1876354092",
            18: "1785043926",
            19: "6178093542",
            20: "5643712089",
            21: "1958627043",
            22: "9572314608",
            23: "0841267953",
            24: "7415038296",
            25: "5364107982",
            26: "1328760549",
            27: "1420698537",
            28: "7368240195",
            29: "8314902567",
            30: "0456897213",
            31: "0954761238",
        }
        return day_of_month_key[datetime.today().day]

    def _md5_hex(self, encode_bytes):
        """HttpImageGetter.encode
        Args:
            encode_bytes: a bytes which is encoded
        Returns:
            A hex string 
        """
        hl = hashlib.md5()
        hl.update(encode_bytes)
        return hl.hexdigest()

    def encrypt(self):
        key = [int(i, 16) for i in self.today_key]  # '123456' ==> [1,2,3,4,5,6]
        return self._md5_hex(RC4(key).engine_crypt(self.passwd.encode()))


class FeiYoungLogin:
    headers = {
        'User-Agent': 'CDMA+WLAN(Maod)',
    }
    def __init__(self, username, password):
        self.username = str(username)
        self.password = str(password)
        self.redirect_url = self._get_redirect_url()
        self.login_url = self._get_login_url()

    def _get_redirect_url(self):
        r = requests.get('http://59.37.96.63:80', allow_redirects=False)
        if r.status_code == 200:    # assert r.status_code == 302
            print('[!]You have successfully connected to the network, Please do not retry.')
            return ''
        return r.headers['Location']    #redirect_url = 'http://58.53.199.144:8001/?userip=100.64.224.167&wlanacname=&nasip=58.50.189.124&usermac=1c-87-2c-77-77-9c'
        #return re.search(r'userip=(\S+?)&wlanacname', self.redirect_url).group(1)

    def _get_login_url(self):
        if not self.redirect_url:
            return ''
        r = requests.get(self.redirect_url + '&aidcauthtype=0', headers=self.headers)
        if r.status_code != 200:
            return ''
        return re.search(r'<LoginURL><!\[CDATA\[(\S+?)\]\]></LoginURL>', r.text).group(1)
    
    def do_login(self):
        postdata = {
            'UserName': '!^Maod0%s' % self.username, 
            'Password': Password(self.password).encrypt(), 
            'createAuthorFlag': 0,
        }
        if not self.login_url:
            return '[!]Get LoginURL Failed'
        r = requests.post(self.login_url, data=postdata, headers=self.headers)
        reply_message = re.search(r'<ReplyMessage>(\S+?)</ReplyMessage>', r.text).group(1)
        logoff_url = re.search(r'<LogoffURL><!\[CDATA\[(\S+?)\]\]></LogoffURL>', r.text).group(1)
        return '[*]ReplyMessage: %s\n[*]LogoffURL: %s' % (reply_message, logoff_url)
    
    @classmethod
    def do_logoff(cls, logoff_url):
        r = requests.get(logoff_url, headers=cls.headers)
        return re.search(r'<LogoffReply>([\s\S]+?)</LogoffReply>', r.text).group(1)

if __name__ == '__main__':
    if len(sys.argv) == 3:
        if sys.argv[1] == 'logoff':
            print(FeiYoungLogin.do_logoff(sys.argv[2]))
        else:
            print(FeiYoungLogin(sys.argv[1], sys.argv[2]).do_login())
    else:
        print('Usage:\n\tLogin:  python the.py yourusername yourpassword\n\tLogoff: python the.py logoff theLogoffURL')